/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package medicalbatch;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import tools.RWConnMgr;
import tools.RWEmail;
import tools.utils.Format;

/**
 *
 * @author rwandell
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            //  Loop through the various databases
            RWConnMgr dbIo = new RWConnMgr("randywandell.com", "chiro_site", "rwtools", "rwtools", RWConnMgr.MYSQL);
            ResultSet lRs = dbIo.opnRS("SELECT * FROM userinfo WHERE secactv");
            while(lRs.next()) {
                if(lRs.getBoolean("sendnotificationemail")) { sendNotificationEmails(lRs.getString("secprf")); }
            }
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void sendNotificationEmails(String dbName) {
        try {
            RWConnMgr io = new RWConnMgr("randywandell.com", dbName, "rwtools", "rwtools", RWConnMgr.MYSQL);
            ResultSet emlRs = io.opnRS("SELECT id, patientid, date, TIME_FORMAT(`time`, '%l:%i %p') as `time`, intervals, missedreason, notified, resourceid, notes  FROM appointments WHERE `date`=DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)");
            while(emlRs.next()) {
                sendAppointmentReminder(io, emlRs.getInt("resourceid"), emlRs.getInt("patientid"), emlRs.getString("date"), emlRs.getString("time"));
            }
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean sendAppointmentReminder(RWConnMgr io, int resourceId, int patientId, String appointmentDate, String appointmentTime) throws Exception {
        boolean messageSent=true;

        String myQuery = "Select resources.id, " +
                "CASE WHEN resources.providerEmailAddress IS NULL OR TRIM(resources.providerEmailAddress)='' THEN environment.officeEmailAddress ELSE resources.providerEmailAddress END AS emailAddress, " +
                "CASE WHEN resources.name IS NULL OR TRIM(resources.name)='' THEN environment.suppliername ELSE resources.name END as emailName, " +
                "CASE WHEN resources.apptEmailSubject IS NULL OR TRIM(resources.apptEmailSubject)='' THEN environment.apptEmailSubject ELSE resources.apptEmailSubject END AS apptEmailSubject," +
                "CASE WHEN resources.apptEmailMessage IS NULL OR TRIM(resources.apptEmailMessage)='' THEN environment.apptEmailMessage ELSE resources.apptEmailMessage END AS apptEmailMessage " +
                "from resources join environment where resources.id=" + resourceId;

//        try {
            ResultSet lRs = io.opnRS(myQuery);
            if(lRs.next()) {
                String fromName="";
                String fromAddress="";

                if(lRs.getString("emailaddress") != null && !lRs.getString("emailaddress").equals("")) {
                    if(lRs.getString("emailAddress") != null) { fromAddress=lRs.getString("emailAddress"); }
                    if(lRs.getString("emailName") != null) { fromName=lRs.getString("emailName"); } else { fromName=fromAddress; }

                    RWEmail email = new tools.RWEmail(fromAddress, fromName);

                    ResultSet pRs = io.opnRS("select firstname, lastname, concat(firstname, ' ',lastname) as name, email from patients where useemail and id=" + patientId);
                    if(pRs.next()) {
                        String content = "text/html";

                        ResultSet emlRs = io.opnRS("select * from chiro_site.emailsettings where id=1");
                        if(emlRs.next()) {
                            String messageText=lRs.getString("apptEmailMessage");
                            String subject = lRs.getString("apptemailsubject");

                            ResultSet aRs = io.opnRS("select * from appointmentreminders where `day`='" + Format.formatDate(appointmentDate, "EEE") + "'");
                            if(aRs.next()) {
                                messageText = aRs.getString("emailmessage");
                                subject = aRs.getString("emailsubject");
                            }
                            aRs.close();

                            messageText = messageText.replaceAll("##PATIENT##", pRs.getString("name"));
                            messageText = messageText.replaceAll("##FIRSTNAME##", pRs.getString("firstname"));
                            messageText = messageText.replaceAll("##LASTNAME##", pRs.getString("lastname"));
                            messageText = messageText.replaceAll("##DOCTOR##", lRs.getString("emailname"));
                            messageText = messageText.replaceAll("##TIME##", appointmentTime);
                            messageText = messageText.replaceAll("##DATE##", Format.formatDate(appointmentDate, "MM/dd/yyyy"));
                            messageText = messageText.replaceAll("##DAY##", Format.formatDate(appointmentDate, "EEE"));

                            email.setContentType(content);
                            if(emlRs.getString("smtphost") != null && !emlRs.getString("smtphost").equals("")) {
                                email.setSMTPHost(emlRs.getString("smtphost"));
                                email.setSMTPUser(emlRs.getString("smtpuser"));
                                email.setSMTPPassword(emlRs.getString("smtppass"));
                            }

                            email.setToName(pRs.getString("name"));
                            email.setToAddress(pRs.getString("email"));
                            email.setSubject(subject);
                            email.setMessage(messageText);
                            email.setAttachmentName("test.xls");
                            email.send();
                        }
                    }
                    pRs.close();
                }
            }
            lRs.close();
//        } catch (Exception e) {
//        }
        return messageSent;
    }

}
